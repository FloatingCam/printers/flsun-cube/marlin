# Marlin

Marlin configuration file used for my FLSun Cube with Cyclops Aqua (2-in 1-out)
mixing head extruder.

![FLSun Cube Cyclops Aqua](FLSunCube-CyclopsAqua.png)

I selected an available pin for controlling the water pump, turning on when the extruder reaches 50C.  I print on glass which precludes using the probe they provided so I manually level the bed.

[Screenshots of the configuration changes](https://gitlab.com/FloatingCam/printers/flsun-cube/marlin/wikis/home) made in Marlin v1.1.9 for my setup.